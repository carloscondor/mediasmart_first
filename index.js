'use strict';

var PORT = 8080;
var USER_ID = 'sewriiojclvkslwr';
// var TEST_BILL = '00028988';

var restify = require('restify');
var _ = require('lodash');
var Consumer = require('./consumer').setUserId(USER_ID);

var debug = require('debug')('app:index');
console.log.bind(debug);

var server = restify.createServer();

server.use(restify.queryParser());

server.get('/total', function (req, res) {
  var query = req.query;

  debug('req.query', req.query);

  Consumer.getBill(query.bill, function (err, bill) {
    if (err) {
      console.error('Error getting bill', err);
      return res.json(500, err);
    }

    debug('Bill received');

    var total_price = _.reduce(bill.lineItems, function (sum, o) {
      return sum + (o.quantity * o.unitPrice);
    }, 0);

    debug('Calculated total price ' + total_price);

    return res.json({
      bill: query.bill,
      total: total_price
    });
  });
})

server.listen(PORT, function() {
  console.log('%s listening at %s', server.name, server.url);
});
