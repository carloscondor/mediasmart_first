'use strict';

var url = require('url');
var request = require('request');
var debug = require('debug')('app:consumer');
console.log.bind(debug);

var API_URL = 'http://billing.mediasmart.mobi/';

/**
 * Convert object to query (string) params
 * @param {Object} obj - Object to serialize
 * @return {string} Object serialize as query string
 */
function serializeString (obj) {
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

/**
 * @class Consumer
 */
function Consumer () {
  this.token = null;
}

/**
 * @param {string} userId - User id to use in auth process
 * @return {object} Return self instance of Consumer
 */
Consumer.prototype.setUserId = function (userId) {
  this.userId = userId;

  debug('User id set to ' + userId);

  return this;
}

/**
 * Standar callback definition
 * @callback callback
 * @param err - Error
 * @param result - Expected result
 */

/**
 *  Get user token from a userId
 * @param {[string]} userId - Optional userId
 * @param {callback} cb - Standar callback with err and token
 */
Consumer.prototype.auth = function (userId, cb) {
  var self = this;
  if (arguments.length === 1) {
    cb = userId;
    userId = null;
  }
  if (self.token !== null) {
    debug('Aready exists token', self.token)
    return cb(null, self.token);
  }

  var auth_url = url.resolve(API_URL, '/api/auth');
  var full_auth_url = auth_url + '?' + serializeString({user: userId || self.userId});

  request.get(full_auth_url, function (err, response, body) {
    if (err) {
      self.token = null;
      console.error('Error getting auth token:', err);
      return cb(Error('Internal error'));
    }

    if (response.statusCode === 401) {
      self.token = null;
      return cb(Error('Invalid user'));
    }

    if (typeof body === 'string') body = JSON.parse(body);

    self.token = body.token;

    debug("Token set to", self.token);

    if (cb) return cb(err, body.token);
  });
};

/**
 * Get bill from billId
 * @param {[string]} billId - Target bill id
 * @param {callback} cb - Standar callback with err and token
 */
Consumer.prototype.getBill = function (billId, cb) {
  var self = this
  self.auth(function(err) {
    if (err) return cb(err);
    var bills_url = url.resolve(API_URL, '/api/bill');
    var full_bill_request = bills_url + '?' + serializeString({token: self.token, bill: billId});

    request.get(full_bill_request, function (err, response, body) {
      if (response.statusCode === 401) {
        debug('Unset token because is invalid');
        self.token = null;
        return cb(Error('Invalid Token. Next time will auth again'));
      } else if (response.statusCode === 404) {
        return cb(Error('Bill not found'));
      }

      if (typeof body === 'string') body = JSON.parse(body);

      return cb(err, body);
    });
  });
};


var Consumer = module.exports = exports = new Consumer()
